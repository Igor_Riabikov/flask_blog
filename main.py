import json
import requests

from flask import Flask, request, Response
from utils import gen_password, parse_length, execute_query, parse_chars

app = Flask(__name__)


@app.route('/')
def hello_world(): # view-функция
    return 'Hello World!'


@app.route('/now')
def get_current_time():
    return str(get_current_time())


@app.route('/password')
def get_random():
    try:
        length = parse_length(request,10)
        chars = parse_chars(request,0)
    except Exception as ex:
        return Response(str(ex), status=400)

    return gen_password(length,chars)


@app.route('/bitcoin')
def get_bitcoin_rate():
    currency = request.args.get("currency","USD")
    response = requests.get('https://bitpay.com/api/rates')
    rates = json.loads(response.text)
    for rate in rates:
        if rate['code'] == currency:
            return "Cource Bitcoin in - "+ currency +" - "+ str(rate['rate'])
    return 'N/A'


@app.route('/customers')
def get_customers():
    records = execute_query('SELECT FirstName, LastName FROM Customers')
    return str(records)

@app.route('/unique_names')
def get_unique_names():
    records = execute_query('SELECT FirstName FROM Customers')
    unique_names=set(records)
    # for i in records:
    #     unique_names.add(i)
    count_unique_names=len(unique_names)

    return "Uniq. names in column 'FirstName' - " + str(count_unique_names)

@app.route('/tracks_count')
def get_tracks_count():
    records = execute_query('SELECT Trackid FROM Tracks')
    return "Tracks count in 'Tracks' -  " + str(len(records))

@app.route('/gross_turnover')
def get_gross_turnover():
    sum_transactions=[]
    records = execute_query('SELECT UnitPrice, Quantity FROM Invoice_Items')
    for i in records:
        transaction = i[0]*i[1]
        sum_transactions.append(transaction)

    return "Total = " + str(sum(sum_transactions))

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8000)