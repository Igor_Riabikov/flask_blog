import datetime
import os
import random
import sqlite3
import string


def execute_query(query):
     db_path = os.path.join(os.getcwd(), 'chinook.db')
     conn = sqlite3.connect(db_path)
     cur = conn.cursor()
     cur.execute(query)
     records = cur.fetchall()
     return records


def parse_chars(request,default=0):
     chars = request.args.get('chars',str(default))
     if not chars.isnumeric():
         raise ValueError("VALUE ERROR: int")

     chars=int(chars)

     return chars


def parse_length(request, default=10):
     length = request.args.get('length', str(default))

     if not length.isnumeric():
         raise ValueError("VALUE ERROR: int")

     length = int(length)

     if not 3 < length < 100:
         raise ValueError("RANGE ERROR: [3..10]")

     return length


def gen_password(length,chars):

     if chars>0:
         symbols_for_password=string.ascii_letters+string.digits
     else:
         symbols_for_password =string.digits

     result = ''.join([
             random.choice(symbols_for_password)
             for _ in range(length)
         ])
     return result


def generate_current_time():
     return datetime.datetime.now()