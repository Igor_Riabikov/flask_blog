from flask import Flask, send_file
from faker import Faker
import random
import csv

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello, World!'

@app.route('/requirements')
def get_requirment():

    #file_data=open('./requirements.txt')
    #for_view=file_data.read()
    #file_data.close()
    return send_file('./requirements.txt')

@app.route('/random_users')
def gen_random_users():
    fake=Faker()
    user_data={}
    for name in range(100):
        user = fake.name()
        mail = fake.email()
        user_data[user]=mail
    print(user_data)
    return user_data

@app.route('/avr_data')
def get_avr_data():
    table_data=open("./hw.csv")
    reader=csv.DictReader(table_data)
    height_list = []
    weight_list = []
    meter_per_inch = 0.254 #Для перевода дюймов в метры
    kg_per_pound = 0.4536 #Для перевода фунтов в килограммы
    for line in reader:
        height=line.get(' "Height(Inches)"')
        weight=line.get(' "Weight(Pounds)"')
        weight=float(weight.strip())
        height=float(height.strip())
        height_list.append(height)
        weight_list.append(weight)
    average_height=sum(height_list)/len(height_list)*meter_per_inch
    average_weight=sum(weight_list)/len(weight_list)*kg_per_pound

    table_data.close()
    return "Average height - "+str(average_height)+" (m), "+"Average weight - "+str(average_weight)+" (kg)"
